#include <iostream>

    
class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Null" << '\n';
    }

};

class Dog :public Animal
{
public:
    void Voice() override
    {
        std::cout << "WOof" << '\n';

    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow" << '\n';

    }
};


int main()
{
    Animal* M[2];
    M[0] = new Cat();
    M[1] = new Dog();

    for (Animal* a : M)
        a->Voice();
}

